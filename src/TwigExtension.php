<?php

namespace Drupal\pc;

/**
 * Class TwigExtension.
 */
class TwigExtension extends \Twig\Extension\AbstractExtension {

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return 'pc';
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return ['pc' => new \Twig\TwigFunction('pc', 'pc')];
  }

}
